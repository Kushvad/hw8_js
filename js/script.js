// Task 1
let elementP = document.getElementsByTagName("p");
for (const elemP of elementP) {
    elemP.style.backgroundColor = '#ff0000';
}

// Task 2
let elementId = document.getElementById("optionsList")
console.log(elementId);
console.log(elementId.parentElement);
let elementNodes = elementId.childNodes;
for (const elemNode of elementNodes) {
    console.log('Назва:',elemNode, "nodeType = " + elemNode.nodeType );
}

// Task3
let elementParagraph = document.createElement("p");
let container = document.querySelector('.header-content-wrapper')
elementParagraph.className = 'testParagraph';
elementParagraph.textContent = 'This is a paragraph';
container.prepend(elementParagraph);

// Task 4,5
let elementClass = document.querySelectorAll('.main-header div');
console.log(elementClass);
for (const elemClass of elementClass ){
    elemClass.classList.add('nav-item');
}
console.log(elementClass);

// Task 6
let elementsSectionTitle = document.querySelectorAll('.section-title');
for (const elemSectionTitle of elementsSectionTitle) {
    elemSectionTitle.classList.remove('section-title');
}





